/*jshint esversion:6*/

var inputBuffer = '';
var overState = {
    preload: function() {
        game.stage.backgroundColor = '#123456';
    },
    create: function() {
        inputBuffer = '';

        game.input.keyboard.onPressCallback = this.processKey;

        if(!isMultiPlayer) {
            this.textGame = game.add.text(game.width * 0.5, game.height * 0.2, `GAME OVER`);
            this.textGame.anchor.setTo(0.5, 0.5);
            this.textGame.font = 'Fredoka One';
            this.textGame.fontSize = 80;
            let grd = this.textGame.context.createLinearGradient(0, 0, 0, this.textGame.canvas.height);
            grd.addColorStop(0, '#91ffe1');   
            grd.addColorStop(1, '#0cad81');
            this.textGame.fill = grd;
            game.add.tween(this.textGame).to({angle: -2}, 500).to({angle: 2}, 1000).to({angle: 0}, 500).loop().start();
            
            var bar = game.add.graphics();
            bar.beginFill(0xffffff, 0.2);
            bar.drawRect(0, game.height * 0.6, 880, 120);
            this.nameLabel = game.add.text(0, 0, `NAME`);
            this.nameLabel.font = 'Fredoka One';
            this.nameLabel.fontSize = 50;
            this.nameLabel.boundsAlignH = "center";
            this.nameLabel.boundsAlignV = "up";
            grd = this.nameLabel.context.createLinearGradient(0, 0, 0, this.nameLabel.canvas.height);
            grd.addColorStop(0, '#7cf981');   
            grd.addColorStop(1, '#249e29');
            this.nameLabel.fill = grd;
            this.nameLabel.setTextBounds(0, game.height * 0.6, 880, 120);

            this.inputField = game.add.text(game.width * 0.5, game.height * 0.72, '');
            this.inputField.anchor.setTo(0.5, 0.5);
            this.inputField.font = 'Fredoka One';
            this.inputField.fontSize = 50;
            this.inputField.fill = '#fcfcfc';

            if(highestScore.text < game.global.score) game.add.audio('high').play();
            this.scoreLabel = game.add.text(game.width * 0.5, game.height * 0.4, `${highestScore.text < game.global.score ? 'NEW BEST SCORE' : 'SCORE'}`);
            this.scoreLabel.anchor.setTo(0.5, 0.5);
            this.scoreLabel.font = 'Fredoka One';
            this.scoreLabel.fontSize = 50;
            grd = this.scoreLabel.context.createLinearGradient(0, 0, 0, this.scoreLabel.canvas.height);
            grd.addColorStop(0, '#ff6060');   
            grd.addColorStop(1, '#c90202');
            this.scoreLabel.fill = grd;

            this.score = game.add.text(game.width * 0.5, game.height * 0.5, `${game.global.score}`);
            this.score.anchor.setTo(0.5, 0.5);
            this.score.font = 'Fredoka One';
            this.score.fontSize = 50;
            this.score.fill = '#fcfcfc';

            this.textSubmit = game.add.text(game.width * 0.5, game.height * 0.9, 'Press ENTER to submit');
            this.textSubmit.anchor.setTo(0.5, 0.5);
            this.textSubmit.font = 'Fredoka One';
            this.textSubmit.fontSize = 35;
            grd = this.textSubmit.context.createLinearGradient(0, 0, 0, this.textSubmit.canvas.height);
            grd.addColorStop(0, '#ffd344');   
            grd.addColorStop(1, '#bf9200');
            this.textSubmit.fill = grd;

            this.scoreRes = game.add.sprite(game.width * 0.15, game.height * 0.45, 'score');
            this.scoreRes.anchor.setTo(0.5, 0.5);
            this.scoreRes.animations.add('lists', [0, 1, 2, 3, 4], 8, true);
            if(bgIdx == 0) {
                if(game.global.score > '100') {
                    console.log("1");
                    this.scoreRes.frame = 0;
                }
                else if(game.global.score > '075')
                    this.scoreRes.frame = 1;
                else if(game.global.score > '050')
                    this.scoreRes.frame = 2;
                else if(game.global.score > '025')
                    this.scoreRes.frame = 3;
                else {
                    console.log("1");
                    this.scoreRes.frame = 4;
                }
            }
            else if(bgIdx == 1) {
                if(game.global.score > '75') {
                    this.scoreRes.frame = 0;
                }
                else if(game.global.score > '050')
                    this.scoreRes.frame = 1;
                else if(game.global.score > '025')
                    this.scoreRes.frame = 2;
                else if(game.global.score > '010')
                    this.scoreRes.frame = 3;
                else {
                    this.scoreRes.frame = 4;
                }
            }
        }
        else {
            this.scoreLabel = game.add.text(game.width * 0.5, game.height * 0.4, `WINNER`);
            this.scoreLabel.anchor.setTo(0.5, 0.5);
            this.scoreLabel.font = 'Fredoka One';
            this.scoreLabel.fontSize = 50;
            grd = this.scoreLabel.context.createLinearGradient(0, 0, 0, this.scoreLabel.canvas.height);
            grd.addColorStop(0, '#ff6060');   
            grd.addColorStop(1, '#c90202');
            this.scoreLabel.fill = grd;

            this.score = game.add.text(game.width * 0.5, game.height * 0.5, `PLAYER${multiLastWinner}`);
            this.score.anchor.setTo(0.5, 0.5);
            this.score.font = 'Fredoka One';
            this.score.fontSize = 50;
            this.score.fill = '#fcfcfc';

            this.textSubmit = game.add.text(game.width * 0.5, game.height * 0.9, 'Press ENTER to MENU');
            this.textSubmit.anchor.setTo(0.5, 0.5);
            this.textSubmit.font = 'Fredoka One';
            this.textSubmit.fontSize = 35;
            grd = this.textSubmit.context.createLinearGradient(0, 0, 0, this.textSubmit.canvas.height);
            grd.addColorStop(0, '#ffd344');   
            grd.addColorStop(1, '#bf9200');
            this.textSubmit.fill = grd;
        }
    },
    update: function() {
        if(!isMultiPlayer) {
            this.inputField.text = inputBuffer;
        }
    },
    processKey: function(key) {
        if(!isMultiPlayer) {
            if(game.input.keyboard.lastKey.keyCode == Phaser.Keyboard.ENTER) {
                if(inputBuffer !== '') {
                    firebase.database().ref(`m_leaderboard${bgIdx}`).push({
                        name: `${inputBuffer}`,
                        score: `${game.global.score}`,
                    });
                    game.state.start('menu');
                }
            }
            else if(game.input.keyboard.lastKey.keyCode == Phaser.Keyboard.BACKSPACE) {
                inputBuffer = inputBuffer.slice(0, -1);
            }
            else {
                inputBuffer += String(key);
            }
            // console.log(key);
        }
        else {
            if(game.input.keyboard.lastKey.keyCode == Phaser.Keyboard.ENTER) {
                game.state.start('menu');
            }
        }
    },
};