/*jshint esversion:6*/

var highestScore;
var highestName;

var multiLastWinner = 1;

var playState = {
    preload: function() {
        
    },
    create: function() {
        game.stage.backgroundColor = '#fcfcfc';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        this.cursor = game.input.keyboard.createCursorKeys();

        if(isMultiPlayer) {
            this.numLeft = game.input.keyboard.addKey(Phaser.Keyboard.NUMPAD_4);
            this.numRight = game.input.keyboard.addKey(Phaser.Keyboard.NUMPAD_6);
        }

        this.platforms = [];
        this.lastTime = 0;
        this.isGameOver = false;
        this.floor = 1;

        this.keyZ = game.input.keyboard.addKey(Phaser.Keyboard.Z);
        
        this.topGroup = game.add.group();
        this.midGroup = game.add.group();
        this.lowGroup = game.add.group();
        
        this.background = game.add.sprite(0, 0, 'background');

        this.modesParams = {
            0: {'Normal': 20, 'Right': 40, 'Left': 60, 'Up': 70, 'Nail': 85},
            1: {'Normal': 10, 'Right': 35, 'Left': 60, 'Up': 65, 'Nail': 80},
        };
        
        this.createTexts();
        this.createBorders();
        this.createPlayer();

        this.gameBackground = game.add.image(0, 100, `bg${bgIdx}`);
        game.world.sendToBack(this.gameBackground);
    },
    update: function() {
        this.gameOver();
        if(this.isGameOver) {
            if(isMultiPlayer) {
                if(multiLastWinner === 1) {
                    game.physics.arcade.collide(this.player, this.platforms, this.platformEffect);
                    game.physics.arcade.collide(this.player, [this.borderLeft, this.borderRight]);
                } else {
                    game.physics.arcade.collide(this.player2, this.platforms, this.platformEffect);
                    game.physics.arcade.collide(this.player2, [this.borderLeft, this.borderRight]);
                }
            }
            return;
        }

        game.physics.arcade.collide(this.player, this.platforms, this.platformEffect);
        game.physics.arcade.collide(this.player, [this.borderLeft, this.borderRight]);

        if(isMultiPlayer) {
            game.physics.arcade.collide(this.player2, this.platforms, this.platformEffect);
            game.physics.arcade.collide(this.player2, [this.borderLeft, this.borderRight]);
        }

        this.overBorderTop();

        this.updateTexts();
        this.updatePlatforms();
        this.updatePlayer();

        game.world.moveUp(this.topGroup);
        game.world.moveUp(this.midGroup);
    },
    updatePlayer: function() {
        this.movePlayer();
        this.animatePlayer();

        let curTime = game.time.now;
        if(this.player.life > 0 && curTime < this.player.lastSuperTime && curTime > this.player.lastSuperTime - 1200) {
            if(!this.player.isHitted) {
                this.hitTween = game.add.tween(this.player).to( { alpha: 0.3 }, 100).to( { alpha: 1 }, 100).yoyo(true).loop().start();
                this.player.isHitted = true;
            }
        }
        else {
            if(this.hitTween)
                this.hitTween.stop();
            this.player.alpha = 1;
            this.player.isHitted = false;
        }

        if(isMultiPlayer) {
            if(this.player2.life > 0 && curTime < this.player2.lastSuperTime && curTime > this.player2.lastSuperTime - 1200) {
                if(!this.player2.isHitted) {
                    this.hitTween2 = game.add.tween(this.player2).to( { alpha: 0.3 }, 100).to( { alpha: 1 }, 100).yoyo(true).loop().start();
                    this.player2.isHitted = true;
                }
            }
            else {
                if(this.hitTween2)
                    this.hitTween2.stop();
                this.player2.alpha = 1;
                this.player2.isHitted = false;
            }
        }
    },
    createPlayer: function() {
        if(isMultiPlayer) {
            this.player = game.add.sprite(220, 400, 'player');
        }
        else {
            this.player = game.add.sprite(250, 400, 'player');
        }
        this.player.anchor.setTo(0.5, 0.5);
        this.player.alpha = 1;
        
        game.physics.arcade.enable(this.player);
        this.player.body.setSize(40, 45, 2.5, 0);
        this.player.body.gravity.y = 600;
        this.player.body.velocity.y = -450;
        this.player.animations.add('leftwalk', [13, 14, 15, 16, 17, 18], 8, true);
        this.player.animations.add('rightwalk', [19, 20, 21, 22, 23, 24], 8, true);
        this.player.animations.add('leftjump', [1, 2, 3, 4, 5, 6], 10, true);
        this.player.animations.add('rightjump', [7, 8, 9, 10, 11, 12], 10, true);
        this.player.animations.add('leftrun', [25, 26, 27, 28, 29, 30], 8, true);
        this.player.animations.add('rightrun', [31, 32, 33, 34, 35, 36], 8, true);

        this.player.life = 5;
        this.player.lastSuperTime = 0;
        this.player.isHitted = false;
        this.player.lastTouchOn = null;
        this.player.isRunning = false;

        this.player.deadSound = game.add.audio('dead');
        this.player.hitSound = game.add.audio('hit');
        this.player.restoreSound = game.add.audio('restore');

        this.midGroup.add(this.player);

        this.player.m_hps = [];
        for(let i = 0; i < this.player.life; ++i) {
            let hp = game.add.sprite(i*40, 60, 'hp');
            hp.animations.add('fire', [0, 1, 2, 3, 4, 5], 8, true);
            hp.animations.play('fire');
            this.player.m_hps.push(hp);
        }

        if(isMultiPlayer) {
            this.player2 = game.add.sprite(280, 400, 'player2');
            this.player2.anchor.setTo(0.5, 0.5);
            this.player2.alpha = 1;

            game.physics.arcade.enable(this.player2);
            this.player2.body.setSize(40, 50, 2.5, 0);
            this.player2.body.gravity.y = 600;
            this.player2.body.velocity.y = -450;
            this.player2.animations.add('leftwalk', [1, 2, 3, 4, 5, 6], 8, true);
            this.player2.animations.add('rightwalk', [7, 8, 9, 10, 11, 12], 8, true);
            this.player2.animations.add('leftjump', [13, 14, 15, 16, 17, 18], 10, true);
            this.player2.animations.add('rightjump', [19, 20, 21, 22, 23, 24], 10, true);
            
            this.player2.deadSound = game.add.audio('dead');
            this.player2.hitSound = game.add.audio('hit');
            this.player2.restoreSound = game.add.audio('restore');

            this.player2.life = 5;
            this.player2.lastSuperTime = 0;
            this.player2.isHitted = false;
            this.player2.lastTouchOn = null;

            this.midGroup.add(this.player2);

            this.player2.m_hps = [];
            for(let i = 0; i < this.player2.life; ++i) {
                let hp2 = game.add.sprite(500 - i*40, 60, 'hp2');
                hp2.anchor.setTo(1, 0);
                hp2.animations.add('fire', [0, 1, 2, 3, 4, 5], 8, true);
                hp2.animations.play('fire');
                this.player2.m_hps.push(hp2);
            }
        }
    },
    movePlayer: function() {
        let cutTime = game.time.now;
        if(this.keyZ.isDown) {
            if(this.floor > 1) {
                this.floor -= 0.1;
                this.player.isRunning = true;
            } else {
                this.player.isRunning = false;
            }
        }
        else {
            this.player.isRunning = false;
        }
        if (this.cursor.left.isDown) {
            if(this.keyZ.isDown) {
                this.player.body.velocity.x = -300;
            } else {
                this.player.body.velocity.x = -200;
            }
        }
        else if (this.cursor.right.isDown) {
            if(!isMultiPlayer && this.keyZ.isDown) {
                this.player.body.velocity.x = 300;
            } else {
                this.player.body.velocity.x = 200;
            }
        }
        else {
            if(!this.player.isRunning)
                this.player.body.velocity.x = 0;
        }

        if(isMultiPlayer) {
            if(this.numLeft.isDown) {
                this.player2.body.velocity.x = -200;
            } else if(this.numRight.isDown) {
                this.player2.body.velocity.x = 200;
            } else {
                this.player2.body.velocity.x = 0;
            }
        }
    },
    animatePlayer: function() {
        let playerVelocity = {x: this.player.body.velocity.x, y: this.player.body.velocity.y};

        if(playerVelocity.x == 0 && playerVelocity.y == 0) {
            this.player.frame = 0;
            this.player.animations.stop();
        }
        else if(playerVelocity.x < 0) {
            if(playerVelocity.y == 0) {
                if(this.player.isRunning) {
                    this.player.animations.play('leftrun');
                }
                else {
                    this.player.animations.play('leftwalk');
                }
            }
            else {
                this.player.animations.play('leftjump');
            }
        }
        else {
            if(playerVelocity.y == 0) {
                if(this.player.isRunning) {
                    this.player.animations.play('rightrun');
                }
                else {
                    this.player.animations.play('rightwalk');
                }
            }
            else {
                this.player.animations.play('rightjump');
            }
        }

        if(isMultiPlayer) {
            let player2Velocity = {x: this.player2.body.velocity.x, y: this.player2.body.velocity.y};

            if(player2Velocity.x == 0 && player2Velocity.y == 0) {
                this.player2.frame = 0;
                this.player2.animations.stop();
            }
            else if(player2Velocity.x < 0) {
                if(player2Velocity.y == 0) {
                    this.player2.animations.play('leftwalk');
                } else {
                    this.player2.animations.play('leftjump');
                }
            }
            else {
                if(player2Velocity.y == 0) {
                        this.player2.animations.play('rightwalk');
                } else {
                    this.player2.animations.play('rightjump');
                }
            }
        }
    },
    updatePlatforms: function() {
        let curTime = game.time.now;

        if(curTime > this.lastTime + 600) {
            this.lastTime = curTime;
            this.createPlatform();
        }

        for(let platform of this.platforms) {
            
            if(platform.goingUp) platform.body.velocity.x = (platform.goingUp < 0.5) ? -50 : 50;
            platform.body.position.y -= 2;

            if(platform.enemy) {
                platform.enemy.body.position.y -= 2;
                if(platform.enemy.facingLeft && (platform.enemy.body.position.x > platform.body.position.x - 25)) {
                    platform.enemy.position.x -= 0.5;
                    if(platform.enemy.position.x < platform.body.position.x - 24.9) {
                        platform.enemy.facingLeft = false;
                        platform.enemy.animations.play('walkright');
                    }
                }
                if(!platform.enemy.facingLeft && (platform.enemy.body.position.x < platform.body.position.x + 25)) {
                    platform.enemy.position.x += 0.5;
                    if(platform.enemy.position.x > platform.body.position.x + 24.9) {
                        platform.enemy.facingLeft = true;
                        platform.enemy.animations.play('walkleft');
                    }
                }
            }
            if(platform.body.position.y < 70) {
                this.topGroup.remove(platform);
                if(platform.enemy) {
                    this.topGroup.remove(platform.enemy);
                }
                this.platforms.splice(platform, 1);
                platform.destroy();
                this.floor += 1;
            }
        }
    },
    createPlatform: function() {
        let platform;
        let pos = {x: Math.random() * (470 - 140) + 30, y: 550};
        let rand = Math.random() * 100;

        if(rand < this.modesParams[bgIdx].Normal) {
            platform = game.add.sprite(pos.x, pos.y, 'platformNormal');
            game.physics.arcade.enable(platform);
            platform.body.setSize(40, 15, 20, 10);
            platform.body.immovable = true;
            platform.animations.add('float', [0, 1, 2, 3, 4, 5], 8, true);
            
            platform.animations.play('float');
        }
        else if(rand < this.modesParams[bgIdx].Right) {
            platform = game.add.sprite(pos.x, pos.y, 'platformRight');
            game.physics.arcade.enable(platform);
            platform.body.setSize(90, 16, 4, 0);
            platform.body.immovable = true;
            platform.animations.add('scroll', [0, 1, 2, 3], 8, true);

            platform.animations.play('scroll');
        }
        else if(rand < this.modesParams[bgIdx].Left) {
            platform = game.add.sprite(pos.x, pos.y, 'platformLeft');
            game.physics.arcade.enable(platform);
            platform.body.setSize(90, 16, 4, 0);
            platform.body.immovable = true;
            platform.animations.add('scroll', [0, 1, 2, 3], 8, true);

            platform.animations.play('scroll');
        }
        else if(rand < this.modesParams[bgIdx].Up) {
            platform = game.add.sprite(pos.x, pos.y, 'platformUp');
            game.physics.arcade.enable(platform);
            platform.body.setSize(40, 15, 20, 10);
            platform.body.immovable = true;
            platform.animations.add('float', [0, 1, 2, 3, 4, 5], 8, true);
            
            platform.animations.play('float');
        }
        else if(rand < this.modesParams[bgIdx].Nail) {
            platform = game.add.sprite(pos.x, pos.y, 'platformNail');
            game.physics.arcade.enable(platform);
            platform.body.setSize(90, 14, 3.5, 16);
            platform.body.immovable = true;
            platform.animations.add('yoyo', [0, 1, 2, 3], 8, true);

            platform.animations.play('yoyo');
        }
        else {
            platform = game.add.sprite(pos.x, pos.y, 'platformEnemy');
            game.physics.arcade.enable(platform);
            platform.body.setSize(60, 15, 20, 10);
            platform.body.immovable = true;
            platform.animations.add('float', [0, 1, 2, 3, 4, 5], 8, true);
            platform.animations.play('float');

            let enemy = game.add.sprite(pos.x + Math.random() * 70 + 20, pos.y - 30, 'enemy1');
            game.physics.arcade.enable(enemy);
            enemy.body.immovable = true;
            enemy.facingLeft = true;
            enemy.animations.add('walkleft', [0, 1, 2, 3, 4, 5], 8, true);
            enemy.animations.add('walkright', [6, 7, 8, 9, 10, 11], 8, true);
            enemy.animations.play('walkleft');
            platform.enemy = enemy;
        }
        
        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;

        this.platforms.push(platform);
        this.topGroup.add(platform);
        if(platform.enemy) {
            this.topGroup.add(platform.enemy);
        }
    },
    platformEffect: function(player, platform) {
        let curTime = game.time.now;
        if(platform.key == 'platformNormal') {

        }
        if(platform.key == 'platformRight') {
            player.body.x += 2;
        }
        if(platform.key == 'platformLeft') {
            player.body.x -= 2;
        }
        if(platform.key == 'platformNail') {
            if(player.life > 0 && curTime > player.lastSuperTime) {
                player.life -= 1;
                player.lastSuperTime = curTime + 1200;
                player.m_hps[player.m_hps.length-1].destroy();
                player.m_hps.pop();
                player.hitSound.play();
            }
        }
        if(platform.key == 'platformEnemy') {
            game.physics.arcade.collide(player, platform.enemy, (p, e) => {
                if(player.life > 0 && curTime > player.lastSuperTime) {
                    p.life -= 1;
                    player.lastSuperTime = curTime + 1200;
                    player.m_hps[player.m_hps.length-1].destroy();
                    player.m_hps.pop();
                    player.hitSound.play();
                }
            });
        }
        if(platform.key == 'platformUp') {
            if(!platform.goingUp) { 
                platform.goingUp = Math.random() + 0.00000001;
                if(player.life < 5) {
                    player.life += 1;
                    let hp = game.add.sprite(player.m_hps.length*40, 60, 'hp');
                    hp.animations.add('fire', [0, 1, 2, 3, 4, 5], 8, true);
                    hp.animations.play('fire');
                    player.m_hps.push(hp);
                    player.restoreSound.play();
                }
            }
        }
    },
    createBorders: function() {
        this.borderTop = game.add.sprite(0, 0+100, 'borderTop');
        this.borderLeft = game.add.sprite(0, 0+100, 'borderLeft');
        this.borderRight = game.add.sprite(470, 0+100, 'borderRight');

        game.physics.arcade.enable(this.borderTop);
        game.physics.arcade.enable(this.borderLeft);
        game.physics.arcade.enable(this.borderRight);

        this.borderLeft.body.setSize(30, 500, 0, -50);
        this.borderRight.body.setSize(30, 500, 0, -50);

        this.borderTop.body.immovable = true;
        this.borderLeft.body.immovable = true;
        this.borderRight.body.immovable = true;

        this.borderTop.animations.add('scroll', [0, 1, 2, 3], 10, true);
        this.borderTop.animations.play('scroll');
    }, 
    overBorderTop: function() {
        let curTime = game.time.now;
        if(this.player.body.y < 75+100) {
            if(!this.isGameOver && curTime > this.player.lastSuperTime) {
                this.player.life -= 1;
                this.player.lastSuperTime = curTime + 1200;
                this.player.m_hps[this.player.m_hps.length-1].destroy();
                this.player.m_hps.pop();
                this.player.hitSound.play();
            }
        }
        if(isMultiPlayer) {
            if(this.player2.body.y < 75+100) {
                if(curTime > this.player2.lastSuperTime) {
                    this.player2.life -= 1;
                    this.player2.lastSuperTime = curTime + 1200;
                    this.player2.m_hps[this.player2.m_hps.length-1].destroy();
                    this.player2.m_hps.pop();
                    this.player2.hitSound.play();
                }
            }
        }
    },
    updateTexts: function() {
        game.world.moveUp(this.p1Life);

        let tmp = String(Math.round(this.floor));
        let n = 3 - tmp.length;
        for(let i = 0; i < n; ++i) {
            tmp = '0' + tmp;
        }

        this.floorDisplay.text = `B${tmp}F`;
        let grd = this.floorDisplay.context.createLinearGradient(0, 0, 0, this.floorDisplay.canvas.height);
        grd.addColorStop(0, '#7ef464');   
        grd.addColorStop(1, '#6dbc5c');
        this.floorDisplay.fill = grd;
    },
    createTexts: function() {
        this.p1Life = game.add.text(5, 0, "1P LIFE");
        this.p1Life.font = 'Fredoka One';
        this.p1Life.fontSize = 45;
        let grd = this.p1Life.context.createLinearGradient(0, 0, 0, this.p1Life.canvas.height);
        grd.addColorStop(0, '#8ED6FF');   
        grd.addColorStop(1, '#004CB3');
        this.p1Life.fill = grd;

        if(isMultiPlayer) {
            this.p2Life = game.add.text(500, 0, "2P LIFE");
            this.p2Life.anchor.setTo(1, 0);
            this.p2Life.font = 'Fredoka One';
            this.p2Life.fontSize = 45;
            grd = this.p2Life.context.createLinearGradient(0, 0, 0, this.p2Life.canvas.height);
            grd.addColorStop(0, '#8ED6FF');   
            grd.addColorStop(1, '#004CB3');
            this.p2Life.fill = grd;
        }

        this.floorDisplay = game.add.text(250, 0, '');
        this.floorDisplay.anchor.setTo(0.5, 0);
        this.floorDisplay.font = 'Fredoka One';
        this.floorDisplay.fontSize = 45;
        grd = this.floorDisplay.context.createLinearGradient(0, 0, 0, this.floorDisplay.canvas.height);
        grd.addColorStop(0, '#7ef464');   
        grd.addColorStop(1, '#6dbc5c');
        this.floorDisplay.fill = grd;

        if(!isMultiPlayer) {
            firebase.database().ref(`m_leaderboard${bgIdx}`).orderByChild('score').once('value').then(snapshot => {
                snapshot.forEach(childSnapshot => {
                    let childData = childSnapshot.val();
                    highestScore.text = childData.score;
                    highestName.text = childData.name;
                    console.log(childData.score);
                });
            }).catch(err => { console.log(err.message); });
            this.scoreLabel = game.add.text(550, 250, `Best Record\n   B        F`);
            this.scoreLabel.font = 'Fredoka One';
            this.scoreLabel.fontSize = 50;
            this.scoreLabel.fill = '#666666';

            highestScore = game.add.text(630, 315, `000`);
            highestScore.font = 'Fredoka One';
            highestScore.fontSize = 50;
            highestScore.fill = '#ea4b60';

            highestName = game.add.text(585, 360, `N/A`);
            highestName.font = 'Fredoka One';
            highestName.fontSize = 50;
            highestName.fill = '#666666';
        }

        this.modeDisplay = game.add.text(550, 100, `Mode\n   ${bgIdx == 0 ? 'Normal' : 'Hard'}`);
        this.modeDisplay.font = 'Fredoka One';
        this.modeDisplay.fontSize = 50;
        this.modeDisplay.fill = '#666666';

        this.abortText = game.add.text(580, 450, `ABORT`);
        this.abortText.font = 'Fredoka One';
        this.abortText.fontSize = 25;
        this.abortText.fill = '#000';

        this.abortBtn = game.add.sprite(580, 480, 'abortBtn');
        this.abortBtn.animations.add('mouseon', [0, 1, 2, 3, 4], 8, true);
        this.abortBtn.inputEnabled = true;
        this.abortBtn.input.useHandCursor = true;
        this.abortBtn.events.onInputOver.add(() => {
            this.abortBtn.animations.play('mouseon');
        }, this);
        this.abortBtn.events.onInputOut.add(() => {
            this.abortBtn.frame = 0;
            this.abortBtn.animations.stop();
        }, this);
        this.abortBtn.events.onInputDown.add(() => {
            if(!game.paused) {
                game.sound.stopAll();
                game.add.audio('abort').play();
                game.state.start('menu');
            }
        }, this);

        this.stoptText = game.add.text(690, 450, `STOP`);
        this.stoptText.font = 'Fredoka One';
        this.stoptText.fontSize = 25;
        this.stoptText.fill = '#000';

        this.stopBtn = game.add.sprite(680, 475, 'stopBtn');
        this.stopBtn.animations.add('mouseon', [0, 1, 2, 3, 4, 5, 6, 7], 8, true);
        this.stopBtn.inputEnabled = true;
        this.stopBtn.input.useHandCursor = true;
        this.stopBtn.events.onInputOver.add(() => {
            this.stopBtn.animations.play('mouseon');
        }, this);
        this.stopBtn.events.onInputOut.add(() => {
            this.stopBtn.frame = 0;
            this.stopBtn.animations.stop();
        }, this);
        this.stopBtn.events.onInputUp.add(() => {
            game.paused = !(game.paused);
        }, this);
    },
    gameOver: function() {
        if(!isMultiPlayer) {
            if(this.player.life < 1 || this.player.body.y > 610) {
                if(!this.isGameOver) {
                    this.player.frame = 0;
                    this.player.animations.stop();
                    this.player.alpha = 1;
                    this.player.body.velocity.x = 0;
                    this.player.body.velocity.y = -200;
                    this.isGameOver = true;
                    this.player.deadSound.play();
                }
                if(this.player.body.y > 613) {
                    game.global.score = String(Math.round(this.floor));
                    let n = 3 - game.global.score.length;
                    for(let i = 0; i < n; ++i) {
                        game.global.score = '0' + game.global.score;
                        console.log(game.global.score);
                    }
                    game.state.start('over');
                }
                // game.time.events.add(Phaser.Timer.SECOND * 3, () => { game.state.start('main'); }, this);
            }
        }
        else {
            if(this.player.life < 1 || this.player.body.y > 610) {
                multiLastWinner = 2;
                if(!this.isGameOver) {
                    this.player.frame = 0;
                    this.player.animations.stop();
                    this.player.alpha = 1;
                    this.player.body.velocity.x = 0;
                    this.player.body.velocity.y = -200;
                    this.isGameOver = true;
                    this.player.deadSound.play();

                    this.player2.alpha = 1;
                    this.player2.body.velocity.x = 0;
                    this.player2.body.velocity.y = 0;
                    this.player2.body.gravity.y = 0;
                }
                if(this.player.body.y > 613) {
                    game.state.start('over');
                }
            }
            else if(this.player2.life < 1 || this.player2.body.y > 610) {
                multiLastWinner = 1;
                if(!this.isGameOver) {
                    this.player2.frame = 0;
                    this.player2.animations.stop();
                    this.player2.alpha = 1;
                    this.player2.body.velocity.x = 0;
                    this.player2.body.velocity.y = -200;
                    this.isGameOver = true;
                    this.player2.deadSound.play();

                    this.player.alpha = 1;
                    this.player.body.velocity.x = 0;
                    this.player.body.velocity.y = 0;
                    this.player.body.gravity.y = 0;
                }
                if(this.player2.body.y > 613) {
                    game.state.start('over');
                }
            }
        }
    },
};