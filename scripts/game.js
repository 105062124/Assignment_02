/*jshint esversion:6*/

var game = new Phaser.Game(880,700, Phaser.AUTO, 'canvas');
WebFontConfig = {
    active: function() { game.time.events.add(Phaser.Timer.SECOND, [loadState.preload, playState.createTexts], this); },

    google: {
      families: ['Fredoka One']
    }
};

var config = {
  apiKey: "AIzaSyAXUhQCKE2leoUVlR8r0Px9nO9yXZ4g9U4",
  authDomain: "software-studio-astwo.firebaseapp.com",
  databaseURL: "https://software-studio-astwo.firebaseio.com",
  projectId: "software-studio-astwo",
  storageBucket: "software-studio-astwo.appspot.com",
  messagingSenderId: "653540223969"
};
firebase.initializeApp(config);

game.global = {score: '000'};
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('record', recordState);
game.state.add('play', playState);
game.state.add('over', overState);
game.state.start('boot');